import os, sys, re

def convertTexToHtml(html):
    html = re.sub(r'\\begin{abstract}', r'\\section*{Abstract}', html)
    html = re.sub(r'\\end{abstract}', r'', html)
    html = re.sub(r'\\section\*{(.*?)}', r'<h3>\1</h3>', html)
    html = re.sub(r'\\subsection\*{(.*?)}', r'<h4>\1</h4>', html)
    html = re.sub(r'\\textit{(.*?)}', r'<i>\1</i>', html)
    html = re.sub(r'\\textbf{(.*?)}', r'<b>\1</b>', html)
    html = re.sub(r'\\underline{(.*?)}', r'<u>\1</u>', html)
    html = re.sub(r'\\hl{(.*?)}', r'<span style="background-color:yellow;">\1</span>', html)
    html = re.sub(r'\\url{(.*?)}', r"<a href='\1'>\1</a>", html)
    html = re.sub(r'\$\\(.*?)\$', r'&\1;', html)
    html = re.sub(r'\\noindent{(.*?)}', r'\1', html)
    html = re.sub(r'\\noindent ', r'', html)
    html = re.sub(r'\\vspace{(.*?)}', r'', html)
    html = re.sub(r'\\label{(.*?)}', r'', html)

    specialCharacterList = []
    for letter in ("a", "e", "i", "\\i", "o", "u", "z", "A", "E", "I", "O", "U"):
        specialCharacterList.append(["\\`%s" % letter, "&%sgrave;" % letter])
        specialCharacterList.append(["\\'%s" % letter, "&%sacute;" % letter])
        specialCharacterList.append(["\\^%s" % letter, "&%scirc;" % letter])
        specialCharacterList.append(["\\~%s" % letter, "&%stilde;" % letter])
        specialCharacterList.append(["\\\"%s" % letter, "&%suml;" % letter])
        specialCharacterList.append(["\\%s " % letter, "&%sslash;" % letter])
        #specialCharacterList.append(["\\%s " % letter, "&%sring;" % letter])

    specialCharacterList.append(["\\%", "%"])
    specialCharacterList.append(["\\&", "&amp;"])
    specialCharacterList.append(["\\#", "#"])
    specialCharacterList.append(["``", "&quot;"])
    specialCharacterList.append(["''", "&quot;"])
    specialCharacterList.append(["---", "&mdash;"])
    specialCharacterList.append(["--", "&ndash;"])
    specialCharacterList.append(["\\clearpage", ""])
    specialCharacterList.append(["\\begin{center}", "<center>"])
    specialCharacterList.append(["\\end{center}", "</center>"])
    specialCharacterList.append(["\\ldots", "..."])

    for char in specialCharacterList:
        html = html.replace(char[0], char[1])

    mathCharacterList = []
    mathCharacterList.append(["^\\circ", "&deg;"])
    for symbol in ["alpha", "beta", "gamma", "delta", "epsilon", "varepsilon", "zeta", "eta", "theta", "vartheta", "gamma", "kappa", "lambda", "mu", "nu", "xi", "o", "pi", "varpi", "rho", "varrho", "sigma", "varsigma", "tau", "upsilon", "phi", "varphi", "chi", "psi", "omega", "iota"]:
        mathCharacterList.append(["\\%s" % symbol, "&%s;" % symbol])

    for char in mathCharacterList:
        html = html.replace("$" + char[0] + "$", char[1])

    lines = [line for line in html.split("\n") if line.strip() != "" and not "\\renewcommand" in line and not "\\setcounter" in line]
    html = ""
    for line in lines:
        if line.startswith("<") and not line.startswith("<b>"):
            html += line + "\n\n"
        else:
            html += "<p>" + line + "</p>\n\n"

    return html
